# Installation of Oracle Database 12c for an OEM respository database

### Creating groups and users

Create an Oracle Inventory group named oinstall.

`# /usr/sbin/groupadd oinstall`

Create two supplementary groups named dba and oper.

`# /usr/sbin/groupadd dba`

`# /usr/sbin/groupadd oper`

Create a user named oracle and set the password.

`# useradd -c "Oracle rdbms " -m -d /home/oracle -g oinstall -G oper,dba -u 1100 -s /bin/bash oracle`

`# passwd oracle`

Create a user named oraoem and set the password.

`# useradd -c "Oracle Enterprise Manager " -m -d /home/oraoem -g oinstall -G oper,dba -u 4050 -s /bin/bash oraoem`

`# passwd oraoem`

### Configuring OS settings

Add the following to `/etc/sysctl.conf`:

```
fs.file-max = 6815744
kernel.msgmni = 2878
kernel.msgmax = 8192
kernel.msgmnb = 65536
kernel.sem = 250 32000 100 142
kernel.shmmni = 4096
kernel.shmall = 1073741824
kernel.shmmax = 4398046511104
net.core.rmem_default = 262144
net.core.rmem_max = 4194304
net.core.wmem_default = 262144
net.core.wmem_max = 1048576
fs.aio-max-nr = 3145728
net.ipv4.ip_local_port_range = 9000 65500
net.ipv4.ip_forward = 0
net.ipv4.icmp_echo_ignore_broadcasts = 1
net.ipv4.conf.all.accept_source_route = 0
kernel.sysrq = 1
net.ipv4.tcp_max_syn_backlog = 4096
```

Include the following lines in `/etc/security/limits.conf`:
```
oracle soft nproc 2047
oracle hard nproc 16384
oracle soft nofile 4096
oracle hard nofile 65536
oracle soft stack 10240

oraoem soft nofile 4096
oraoem hard nofile 65536
```

### Installing missing packages

Install **libaio-devel-0.3.109-13.el7.i686** and **libaio-devel-0.3.109-13.el7.x86_64** for CentOS 7 from [RPMfind](https://www.rpmfind.net/linux/rpm2html/search.php?query=libaio-devel)

Install **ksh**.

`# yum install ksh`

Install **glibc-devel.i686**.

`# yum install glibc-devel.i686`

### Configuring the host file

If the host is not `localhost`, the hostname needs to be configured in `/etc/hosts`.

### Installing Oracle Database

Download Oracle Database 12.2.0 from Oracle's website.

Move the downloaded zip-file to a suitable staging-directory for unzipping where user `oracle` can have access. In this guide, a directory `/spare` is created for this purpose.

`# mkdir /spare`

`# cd /spare`

`# mv /home/user/Downloads/linuxx64_12201_database.zip .`

`# unzip linuxx64_12201_database.zip`

`# chown -R oracle:oinstall /spare/database`

Create the `oraInventory` directory if it does not already exist and give ownership to user `oracle`.

`# mkdir -p /home/oraInventory`

`# chown -R oracle:oinstall /home/oraInventory`

Create a base directory for the database and change the ownership to `oracle`. In this guide, `/home/oracle/app/dboracle` is created.

`# mkdir -p /home/oracle/app/dboracle`

`# chown -R oracle:oinstall /home/oracle`

Give permission to all users to run X Window System.

`# xhost +`

Switch to user `oracle`.

`# su oracle`

Change directory to `/spare/database`.

`$ cd /spare/database`

Set environment variables and run the install script (note that the environment variables are temporary and will need to be set again in the future).

`$ export ORACLE_BASE=/home/oracle/app/dboracle`

`$ export TEMP=/tmp`

`$ ./runInstaller`

1. Select *Install database software only* and click *Next*.

2. Select *Single instance database installation* and click *Next*.

3. Select *Enterprise Edition* and click *Next*.

4. Make sure the locations are specified correctly and click *Next*. In this guide, Oracle base is `/home/oracle/app/dboracle` and Software location is `/home/oracle/app/dboracle/product/12.2.0/dbhome_1`.

5. Change the default Inventory Directory to the one created earlier: `/home/oraInventory`, and click *Next*.

6. Click *Next*.

7. If prerequisite checks fail click on *Fix & Check Again*, follow instructions, and press OK when done.

8. Click *Install*.

9. Follow instructions and click *Close* after installation has finished.

### Creating an OEM respository database

Create a directory for storing database files and change owner to user `oracle`. In this guide `/home/oracle/app/dborafiles` is created.

`$ mkdir /home/oracle/app/dborafiles`

Switch to user `oracle` if you aren't already.

`# su oracle`

Change directory, set the environment variable, and run the database configuration assistant (DBCA).

`$ cd /home/oracle/app/dboracle/product/12.2.0/dbhome_1/bin`

`$ export TEMP=/tmp`

`$ ./dbca`

1. Select *Create a database* and click *Next*.

2. Change *Global database name* to `oem12c`.

3. Change *Database files location* to the directory you created. In our case it is `/home/oracle/app/dborafiles`.

4. Set an administrative password.

5. Uncheck *Create as Container database*.

6. Click *Next*.

7. Click *Finish* to begin the creation of the database.

8. Click *Close* after it has finished.

Start netca to configure a listener.

`./netca`

10. Select *Listener configuration* and click *Next*.

11. Select *Add* and click *Next*.

12. Set *Listener name* to `LISTENER` and click *Next*.

13. Have TCP as the only selected protocol and click *Next*.

14. Select *Use the standard port number of 1521* and click *Next*.

15. Select *No* and click *Next*.

16. Click *Next*.

17. Click *Finish* to exit the configuration assistant.

Create a file `tnsnames.ora` in `$ORACLE_HOME/network/admin` (in our case `/home/oracle/app/dboracle/product/12.2.0/dbhome_1/network/admin`) with the following content:

```
oem12c =
  (DESCRIPTION =
    (ADDRESS_LIST =
      (ADDRESS =
        (COMMUNITY = SVMUSER.world)
        (PROTOCOL = TCP)
        (Host = localhost)
        (Port = 1521)
      )
    )
    (CONNECT_DATA =
      (SID = oem12c)
      (GLOBAL_NAME = oem12c)
    )
  )
```
**Important:** Replace `localhost` with the correct hostname if you aren't running on `localhost`.

Export environment variables and check to see if listener has been created successfully and the database is reachable.

`$ export ORACLE_SID=oem12c`

`$ export ORACLE_HOME=/home/oracle/app/dboracle/product/12.2.0/dbhome_1`

`$ ./lsnrctl status`

`$ ./tnsping oem12c`

### Configuring the database

Connect to the database with the `sqlplus` script in `$ORACLE_HOME/bin`, using `sys as sysdba` as username.

`./sqlplus`

Run the following query:

```
SQL> set lines 300
SQL> col tablespace_name form a16
SQL> col file_name form a37
SQL> select tablespace_name, file_name, bytes/1024/1024 as SIZE_MB, autoextensible from dba_data_files
Union
select tablespace_name, file_name, bytes/1024/1024 as SIZE_MB, autoextensible from dba_temp_files;
```
Note the name and path of the `UNDOTBS1` file and resize it to 512M.

`SQL> alter database datafile '/home/oracle/app/dborafiles/OEM12C/datafile/o1_mf_undotbs1_dssjq1r0_.dbf' resize 512M;`

Run the following queries as well:

`SQL> alter system set shared_pool_size=600m scope=spfile;`

`SQL> alter system set processes=300 scope=spfile;`

`SQL> alter system set session_cached_cursors=200 scope=spfile;`

`SQL> alter system set db_securefile='PERMITTED' scope=spfile;`

A security fix in 12.2 will cause an error `ORA-28104: input value for statement_types is not valid`. To prevent this, run the following query:

`SQL> alter system set "_allow_insert_with_update_check"=TRUE scope=spfile;`

The allowed logon version needs to be set to 11 because Enterprise Manager 12 actually uses Weblogic 11. Create the file `sqlnet.ora` in `$ORACLE_HOME/network/admin` if it doesn't exist already, and insert the following line in the file:

`SQLNET.ALLOWED_LOGON_VERSION=11`

Restart the database service.

`SQL> shutdown immediate;`

`SQL> startup`

Run the following queries:

`SQL> alter user sys identified by [sys_password];`

`SQL> alter user system identified by [sys_password];`

**Important:** Replace `[sys_password]` with the actual password.

# Installation of Oracle Enterprise Manager 12c

There are two installation options for Enterprise Manager: Simple and Advanced. This guide goes through the simple installation.

Uninstall OpenJDK.

`# yum remove *jdk*`

Download and install Oracle JDK 6 from the Oracle website and set the JAVA_HOME and PATH environment permanently in `/etc/profile` by adding the following lines:

```
export JAVA_HOME=/usr/java/default
export PATH=$JAVA_HOME/bin:$PATH
```
**Important:** The JAVA_HOME path is the parent of the `/bin/` directory listed by the following command:

`ls -l $(which java)`

Activate the changes in `/etc/profile`.

`source /etc/profile`

Download Oracle Enterprise Manager 12.1.0.5 from Oracle's website.

Unzip the files in a suitable directory owned by user `oraoem`.

`# mkdir /home/oraoem/software`

`# cd /home/oraoem/software`

`# mv /home/user/Downloads/em12105* .`

`# chown -R oraoem:oinstall /home/oraoem/software`

`# xhost +`

`# su oraoem`

`$ unzip em12105_linux64_disk1.zip`

`$ unzip em12105_linux64_disk2.zip`

`$ unzip em12105_linux64_disk3.zip`

Create directories `middleware`, `agent`, and `swlib` owned by `oraoem` in a suitable installation location.

`$ mkdir -p /home/oraoem/app/middleware`

`$ mkdir /home/oraoem/app/agent`

`$ mkdir /home/oraoem/app/swlib`

Export the environment variable and run the installation script.

`$ export TEMP=/tmp`

`$ ./runInstaller`

1. Uncheck the *I wish to receive updates via My Oracle Support* checkbox and click *Next*.

2. Select *Skip* and click *Next*.

3. Click *Next*.

4. Select *Simple* and click *Next*.

5. Choose the directories `middleware` and `agent` as *Middleware Home Location*  and *Agent Base directory* and click *Next*.

6. Create an administrator password.

7. Enter hostname, port number, SID, and sys password (in our case `localhost`, `1521`, `oem12c`, `[sys_password]`).

8. Choose the `swlib` directory as *Software Library Location* and click *Next*.

9. Click *Yes* on the Error box so that the installer takes care of the errors.

10. Click *Install*.

11. When you get the error `Error in invoking target 'install' of makefile '/home/oraoem/app/middleware/Oracle_WT/webcache/lib/ins_calypso.mk'`, fix it by adding `-ldms2` to `/home/oraoem/app/middleware/Oracle_WT/lib/sysliblist`.

12. Follow instructions and click *Close* after installation has finished.